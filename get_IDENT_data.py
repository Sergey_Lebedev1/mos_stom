from tqdm import tqdm
from connect_mssql import connect_mssql
import traceback
import sys

def process_ident():
    conn = connect_mssql()
    cursor = conn.cursor(as_dict=True)

    return conn

def trans_csv(csv_name):
    import csv

    with open(csv_name, newline='') as f:
        reader = csv.DictReader(f, delimiter=';')
        pats = list(reader)

    conn = connect_mssql()
    curs = conn.cursor()

    for ii in tqdm(pats):
        spec = '\ufeffID'
        try:
            curs.execute(f"""
            insert into pz.dbo.paymentsin
            (id
            , datetimepayment
            , id_patients
            , payment
            )       
             values (
            {ii[spec]}
            , '{ii['DateTimePayment']}'
            , '{ii['ID_Patients']}'
            , '{ii['Payment']}'
            ); 
            """)
            conn.commit()
        except:
            traceback.print_exc(file=sys.stdout)
            pass
    pass
    return None

def main():
    conn = process_ident()
    return None


if __name__ == '__main__':
    main()

    '''
    cursor.execute(f"""
    BULK INSERT pz.dbo.patients
        FROM '{pat_file}'
        WITH (format = 'CSV', fieldterminator = ',', rowterminator = '\n');
        """)
    '''


    '''
            pat_file = '/home/sergey/Downloads/IDENT_payments.csv'
            trans_csv(pat_file)

                insert into pz.dbo.persons
            (id
            , surname
            , name
            , mobilephonestring
            )       
             values (
            {ii[spec]}
            , N'{ii['Surname']}'
            , N'{ii['Name']}'
            , '{ii['MobilePhoneString']}'
            );        

    
    
    
    
    1938557166
Canada_2030

Доступ к SQL базе 
Логин: readonly_user
Пароль: 6Pvf10T4jQp8EY2Xzmu8G9Zn336gPX'''
