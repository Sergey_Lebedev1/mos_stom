import os.path
import requests
import json
# from connect_firebird import connect_firebird
from tqdm import tqdm
import pymssql
import pyodbc
from datetime import datetime
from pprint import pprint


ff = open('roi.ini')
ini = json.load(ff)
API_key = ini['api_key']
Project_id = ini['project_id']
Db_user = ini['db_user']
Db_password = ini['db_password']
Roi_api_url = 'https://cloud.roistat.com/api/v1/project/'
Suffics = f'key={API_key}&project={Project_id}'
J_file = ini['archive_file']

# second_project = '267762'
# sec_api = 'dff5ff4dd2b1db6570f8037d96d758d8'

def connect_mssql():
    '''
    conn = pymssql.connect(
        server='DESKTOP-DKCV5JO\\PZSQLSERVER',  #   127.0.0.1:15000
        user=Db_user,
        password='6Pvf10T4jQp8EY2Xzmu8G9Zn336gPX',
        database='PZ',
        as_dict=True
        )
    '''

    conn = pyodbc.connect('DRIVER={SQL Server};SERVER=DESKTOP-DKCV5JO\\PZSQLSERVER;DATABASE=PZ;UID=readonly_user;PWD=6Pvf10T4jQp8EY2Xzmu8G9Zn336gPX')
    
    return conn


# Получаем список ранее необработанных заявок из роистат
def get_roi_list_ident():    # roi_id varchar(99), phone varchar(99), update_date varchar(99), revenue int
    conn = connect_mssql()

    # Проверяем есть ли записи в roi_processed
    roi_processed = list()

    if os.path.exists(J_file):
        jf = open(J_file)
        rp = json.load(jf)
        roi_processed = sorted(
            rp, key=lambda x: x['DateTimePayment'], reverse=True)
        latest_date = roi_processed[0]['DateTimePayment']
    else:
        pass
        latest_date = '0000-01-01T00:00:00+0000'

    # latest_date = '2024-07-04T00:00:00+0000'
    # 79520581820
    filters = json.dumps({
    "filters":[
            ["creation_date", ">", latest_date]
            ]
              })
    print('Obtaining orders list from Roistat API')
    #
    # https://cloud.roistat.com/api/v1/project/integration/order/{orderId}/goal/update
    # second_project = '267762'
    # sec_api = 'dff5ff4dd2b1db6570f8037d96d758d8'
    # list_url = Roi_api_url + f'integration/order/list?key={sec_api}&project={second_project}'
    list_url = Roi_api_url + f'integration/order/list?{Suffics}'
    resp = requests.post(list_url, data=filters)
    ans = json.loads(resp.text)

    return (ans['data'], roi_processed)


# Выбираем из БД записи, относящиеся к новым записям в роистате
def new_payments():
    res = get_roi_list_ident()
    new_orders = res[0]
    new_phones = list()
    test_orders = list()
    c = 0
    for ord in tqdm(new_orders, desc='Getting IDS from API'):
        if 'phone' in ord['custom_fields']:
            '''
            if ord['custom_fields']['phone'] != '79114701548':
                c += 1
                continue
            '''
            new_phones.append(ord['custom_fields']['phone'])
            if 'email' in ord[
                'custom_fields'] and len(ord['custom_fields']['email']) > 0:
                new_orders[c]['email'] = ord[
                    'custom_fields']['email']
            else:
                new_orders[c]['email'] = 'nomail@ident.ru'
            new_orders[c]['name'] = ord['custom_fields']['name']
            new_orders[c]['phone'] = ord['custom_fields']['phone']
            test_orders.append(ord)
        elif 'phone' not in ord[
            'custom_fields'] and 'Звонок от' in ord['custom_fields']['order_name']:
            phone = ord[
                'custom_fields']['order_name'].replace(
                'Звонок от', '').strip()
            '''
            if phone != '79114701548':
                c += 1
                continue
            '''
            new_orders[c]['name'] = ord['custom_fields']['order_name']
            new_orders[c]['email'] = 'nomail@ident.ru'
            new_orders[c]['phone'] = phone
            # --------------
            test_orders.append(ord)
            new_phones.append(new_orders[c]['phone'])
        else:
            '''
            c += 1
            continue
            '''
            roi_url = f"{Roi_api_url}orders/{ord['client_id']}/info?key={API_key}&project={Project_id}"
            resp = requests.get(roi_url)
            result = json.loads(resp.text)
            if 'order' not in result:
                continue
            order = result['order']
            if order['client_phones'] == []:
                continue
            if order['client_phones'][0][0] == '+':
                order['client_phones'][0] = order['client_phones'][0][1:]
            new_phones.append(order['client_phones'][0])
            if len(order['client_emails']) > 0:
                new_orders[c]['email'] = order['client_emails'][0]
            else:
                new_orders[c]['email'] = 'nomail@ident.ru'
            new_orders[c]['name'] = order['client_name']
            new_orders[c]['phone'] = order['client_phones'][0]
        c += 1

    conn = connect_mssql()
    curs = conn.cursor()
    curs.execute(f"""
        select 	
            persons.id as client_id
            , persons.surname + ' ' + persons.name as name
		    , persons.MobilePhoneString as phone
		    , pay.ID as payment_id
		    , pay.payment 
		    , pay.DateTimePayment
		    , persons.Email as email 
	    from pz.dbo.persons persons
        inner join pz.dbo.paymentsin pay
        on persons.id = pay.ID_Patients 
        order by persons.ID, pay.DateTimePayment;
    """)
    columns = [column[0] for column in curs.description]
    all_payments = list()
    all_ = curs.fetchall()
    for row in all_:
        all_payments.append(dict(zip(columns, row)))

    # Выбираем записи, соответствующие скаченным из роистат
    new_payments = list()
    np = []
    all_payments_ids = list()
    prevs_payments = [r['payment_id'] for r in res[1]]
    c = 0
    pay = dict()
    for pay in tqdm(
            all_payments, desc='Payments for new phones'):
        paym_date = pay['DateTimePayment']
        all_payments_ids.append(pay['payment_id'])
        all_payments[c]['payment'] = int(pay['payment'])
        all_payments[c]['DateTimePayment'] = pay[
            'DateTimePayment'].strftime('%Y-%m-%dT%H:%M:%S')
        if pay['phone'] in new_phones and pay[
            'payment_id'] not in prevs_payments :
            # Need to find phone corresponding entry in roi array
            name_rec = list()
            for nr in new_orders:
                if 'phone' in nr and pay['phone'] == nr['phone']:
                    name_rec.append((nr['name']
                         , nr['email']
                         , nr['roistat']
                         , nr['creation_date']))

            if datetime.strptime(name_rec[0][3].split('+')[0],
                                 '%Y-%m-%dT%H:%M:%S') > paym_date:
                    c += 1
                    continue
            pay['name'] = name_rec[0][0]
            pay['email'] = name_rec[0][1]
            pay['source'] = name_rec[0][2]
            pay['timestamp'] = datetime.now().strftime(
                '%Y-%m-%dT%H:%M:%S')
            np.append(pay)
            o = 0
        c += 1
        o = None

    return np, all_payments, res[1]


def roistat_insert(new_pays=None, all_payments=None, all_processed=None):
    nn = new_payments()
    # nn[0] - new orders
    # nn[1] - all payments
    # nn[2] - previos payments
    counter = 0
    for order in tqdm(nn[0], desc='Inserting into roistat new orders'):
        # ============================================================
        try:
            lc = json.dumps({
            "creation_date": order['DateTimePayment'],
            # "email": order['email'],
            "name": order['name'],
            "paid_date": order['DateTimePayment'],
            "phone": order['phone'],
            "price": int(order['payment']),
            "source": order['source'],  # "IDENT",
            "status": "1",
            "title": "Платеж по ранее принятой заявке",
            "timestamp": order['timestamp']
            }, ensure_ascii=False)
        except:
            lc = json.dumps({})
        a = requests.post(
            'https://cloud.roistat.com/api/v1/project/leads/lead/create?'
                                  f'key={API_key}&project={Project_id}',
                                  data=lc
        )
        q = None

    old_payments = nn[0] + nn[2]
    # updating json archive
    i = 0
    old_phones = list()
    old_pays = list()
    for pay in old_payments:
        old_payments[i]['payment'] = int(pay['payment'])
        old_phones.append(pay['phone'])
        old_pays.append(pay['payment_id'])
        i += 1

    new_old_pays = list()
    for p in nn[1]:  #
        if p['phone'] in old_phones and p[
            'payment_id'] not in old_pays:
            p['payment'] = int(p['payment'])
            p['timestamp'] = datetime.now().strftime(
                '%Y-%m-%dT%H:%M:%S')
            # Trying to find telephone number
            for ord in nn[2]:
                if ord['phone'] == p['phone']:
                    if 'source' in ord:
                        p['source'] = ord['source']
                    else:
                        p['source'] = 'no data'
                    if p['DateTimePayment'] < ord['DateTimePayment']:
                        date_fl = False
                    else:
                        date_fl = True
            if date_fl:
                new_old_pays.append(p)

    # order['DateTimePayment'].strftime('%Y-%m-%dT%H:%M:%S')
    for order in tqdm(new_old_pays, desc='Roistat old new orders'):
        try:
            if len(order['email']) == 0:
                order['email'] = 'nomail@ident.ru'
            if 'source' in order:
                sour = order['source']
            else:
                sour = 'no data'
            lc = json.dumps({
                "creation_date": order['DateTimePayment'],
                # "email": order['email'],
                "name": order['name'],
                "paid_date": order['DateTimePayment'],
                "phone": order['phone'],
                "price": int(order['payment']),
                "source": sour,
                "status": "1",
                "title": "Платеж по ранее принятой заявке",
                "timestamp": order['timestamp']
            }, ensure_ascii=False)

        except:
            lc = json.dumps({})
        a = requests.post(
            'https://cloud.roistat.com/api/v1/project/leads/lead/create?'
                                  f'key={API_key}&project={Project_id}',
                                  data=lc)
        qq = False

    old_payments += new_old_pays
    if os.path.exists(J_file):
        try:
            os.remove('roistat_processed.bak')
        except:
            pass
        os.rename(J_file, 'roistat_processed.bak')
    c = 0
    for pay in old_payments:
        if isinstance(pay['DateTimePayment'], datetime):
            old_payments[c]['DateTimePayment'] = pay[
                'DateTimePayment'].strftime(
                '%Y-%m-%dT%H:%M:%S')
        c += 1
    with open(J_file, 'w') as file:
        json.dump(old_payments, file)

    return None


def fill_arch():
    roi_processed = list()
    with open(J_file, 'w') as file:
        json.dump(roi_processed, file)

    ans = list()
    c = 0
    for p in ans['data']:
        jjj = p.get('custom_fields')
        if p:
            o_n = jjj.get('order_name')
            if o_n == 'Звонок от 79114701548':
                pprint(p)
        c += 1

    c = 0
    for p in roi_processed:
        if p['phone'] == '79114701548':
            del roi_processed[c]
        c += 1

    jf = open(J_file)
    processed = json.load(jf)
    c = 0
    for p in processed:
        if p['phone'] == '79114701548':
            pprint(p)
        c += 1

    return

def main():
    # new_payments()
    roistat_insert()

    return None


if __name__ == '__main__':
    main()

