from connect_firebird import connect_firebird
from roistat_process import get_roi_list_med

def create_roistat_table():
    conn = connect_firebird()
    conn.cursor().execute("""
        create table roistat_processed (
            roi_id varchar(99)
            , phone varchar(99)
            , update_date varchar(99)
            , payment_id varchar(99)
            , payment_date varchar(99)
            , revenue int
            );
        """)
    conn.cursor().execute("""commit;""")
    return None

def main():
    create_roistat_table()
    return None


if __name__ == '__main__':
    main()