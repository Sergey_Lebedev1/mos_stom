from connect_mssql import connect_mssql


def update_processed(order):
    conn = connect_mssql()
    curs = conn.cursor()

    curs.execute(f"""
            insert into pz.dbo.roistat_processed (
            roi_id
            , phone
            , update_date
            , payment_id
            , payment_date
            , revenue)
            values('{order['client_id']}'
                    , '{order['phone']}'
                    , '{order['DateTimePayment'].strftime(
                                            '%Y-%m-%dT%H:%M:%S')}'
                    , '{order['payment_id']}'
                    , '{order['DateTimePayment'].strftime(
                                            '%Y-%m-%dT%H:%M:%S')}'
                    , {order['payment']}
                    );
    """)
    conn.commit()
    return None


def main():
    pass
    return None


if __name__ == '__main__':
    main()