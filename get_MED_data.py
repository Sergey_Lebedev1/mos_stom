from connect_firebird import connect_firebird as con_fire
from tqdm import tqdm
import os
import firebirdsql
import json
import requests

API_key = 'f57d1f8eaff97b35f51203d3bb428416'
Project_id = '99779'
# https://pass.markonline.ru/l/9LwxkdXOktlBcz7DFUW7n16LicBRjuN2Amn
Roi_api_url = 'https://cloud.roistat.com/api/v1/project/'
Suffics = f'key={API_key}&project={Project_id}'
J_file = '/tmp/roistat_processed.json'

# C:\Users\User\AppData\Local\Programs\Python\Python310\Lib\ctypes\__init__.py
def connect_firebird():
    ex = os.path.exists('C:\\Users\\User\\Downloads\\Telegram Desktop\\MED.gdb')
    conn = firebirdsql.connect(
        host='localhost',
        database='C:\\Users\\User\\Downloads\\Telegram Desktop\\MED.gdb',
        port=3050,
        user='sysdba',
        password='masterkey',
        charset='iso8859_1'
    )


def get_roi_list_med():
    # Ловим и фиксируем последнюю обработанную дату в roistat_processed
    conn = ""  # connect_mssql()
    curs = conn.cursor()
    curs.execute("""
        select * from roistat_processed order by update_date desc ;""")
    roi_processed = curs.fetchall()
    conn.close()
    # roi_id varchar(99), phone varchar(99), update_date varchar(99), revenue int
    # Проверяем есть ли записи в roi_processed
    if len(roi_processed) == 0:
        latest_date = '0000-01-01T00:00:00+0000'
    else:
        latest_date = roi_processed[0][2]

    filters = json.dumps({
    "filters":[
            ["creation_date", ">", latest_date]
            ]
              })
    print('Obtaining orders list from Roistat API')
    list_url = Roi_api_url + f'integration/order/list?{Suffics}'
    resp = requests.post(list_url, data=filters)
    ans = json.loads(resp.text)

    roi_phones = [ii[1] for ii in roi_processed]
    roi_revenues = [ii[3] for ii in roi_processed]
    roi_dates = [ii[2] for ii in roi_processed]
    new_order = list()
    for rec in tqdm(enumerate(ans['data']), desc='Проверяем наличие телефона в базе'):
        # Если телефон в базе обработанных
        if (rec[1]['custom_fields']['phone'] in roi_phones and
                rec[1]['update_date'] in roi_dates):  # Проверять по дате???
            continue
        new_order.append(rec[1])

    conn = connect_firebird()
    curs = conn.cursor()
    counter = 0
    # Updating roi_processed table
    for order in tqdm(ans['data'], desc='Updating roi_processed data'):
        # avoid firebird limitation for memory allocated
        if counter >= 10000:
            conn.close()
            conn = connect_firebird()
            curs = conn.cursor()
            counter = 0
        curs.execute(f"""
            insert into roistat_processed (roi_id
                                        , phone
                                        , update_date
                                        , revenue)
            values('{order['id']}'
                    , '{order['custom_fields']['phone']}'
                    , '{order['update_date']}'
                    , {order['revenue']}
                    );
                """)
        conn.commit()
        counter += 1
    conn.close()

    return new_order

def get_upd_data():
    unproc_roi = get_roi_list_med()

    conn = con_fire()
    curs = conn.cursor()
    base_tables = ['WORKPACIENT',
                   'PAC_DR']

    found_list = list()
    for jj in tqdm(unproc_roi, desc='Ищем в базе записи для обновления Roistat'):
        curs.execute(f"""select sum(wp.SUMMAK) 
                        from {base_tables[0]} wp
                        inner join {base_tables[1]} pd on wp.num_pacient=pd.idpac_pacdr
                        where pd.tel_mob_pacdr='{jj['custom_fields']['phone']}';""")
        res = curs.fetchone()
        if res[0] is None:
            continue
        found_list.append([jj, res[0]])
    conn.close()

    return None


def main():
    get_upd_data()
    return None


if __name__ == '__main__':
    main()



'''
+79281084222
4222

1. Этап
    сбор заявок с roistat за день по апи
        - телефон за соответствующую дату + время
    сохранения полученных уникальных заявок во внутреннюю таблицу
2. Формирование списка уникальных записей с ненулевыми суммами
        - телефон, сумма, дата

TODO способ передачи в роистат, последовательный или пакетный
как реализовать запуск по расписанию в винде

3. Передача в роистат полученных данных
4. проверка консистентности
5. оповещение в телеге: телефон/сумма (из роистат)
6. оформление документации
        



WID
NUM_PACIENT
COMMENT
DATEWORK
PRICE
PRICE_END
ID_DOCTOR
VID
KODVAL
STR_ORG
ORG
ID_SOTR
DATEEDT
TIMEEDT
KODBOL
FIL_WP
ID_NAPRAVL
SKIDKA
NACENKA
KOLVO
SUMMAP
SUMMAK
ID_POLIS_WP
KKSOTR_WP
TYPEPROV_WP
IDPX_WP
NZUB_WP
ID_ASSIST
TYPE_WP
IDPRICE_WP
ID_WORK
KK1SOTR_WP
KK2SOTR_WP
KK3SOTR_WP
PRICE3_WP
IDDISCCARD_WP
DISCZP_WP
TYPEPRICE_WP


select SUM(SUMMAK) as summat from workpacient wp
inner join PAC_DR pd on wp.num_pacient=pd.idpac_pacdr
where pd.tel_mob_pacdr='89092881111'

select SUM(SUMMAK) as summat from workpacient wp
inner join PAC_DR pd on wp.num_pacient=pd.idpac_pacdr
where pd.tel_mob_pacdr='89092881112'


'''


